import greenfoot.*;  //(World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Class that serves a blueprint for each of the bombs that spawn. Includes a timer,
 * and a word associated with each bomb.
 * 
 * @Ryan Nish
 * @version (a version number or a date)
 */
public class Bomb extends Actor
{
    private int count = 0;     //Counts how many acts have occurred, starting at zero
    private double timer;     //Tracks how long the bomb has before it explodes
    private String word;     //The word associated with the bomb
    private String time = "";     //Created a blank time String to display how long before the bomb explodes
    Label displayTime = new Label(time, 38);     //Creates a label for the time before the bomb explodes
    Label displayWord = new Label(word, 50);     //Creates a label for the word associated with the bomb
    GifImage bombImage = new GifImage("bombgif.gif");     //Creates a bomb gif image

    /**
     * Constructor for the Bomb class
     * Takes a word as a parameter
     * Displays the bomb's word
     */
    public Bomb(String word)
    {
        this.word = word;
        timer = word.length()* 40 + 80; //Creates a timer based on a constant and the length of the word
        displayWord.setImage(new GreenfootImage(word, 38, Color.BLACK, Color.WHITE)); //Sets the image displaying the word associated with the bomb
    }

    public void act() 
    {
        if(((Level)getWorld()).easterEgg())
        {
           this.setImage("yodel.jpg");
        }
        else
        {
             setImage(bombImage.getCurrentImage());     //Sets the bomb's image to the bomb gif image
        }
        if(count == 0)
        //Adds in the labels for the bomb if it's the first act 
        {
            ((Level)getWorld()).addObject(displayTime, this.getX() - 2, this.getY() + 42);     //Adds the time label into the world
            ((Level)getWorld()).addObject(displayWord, this.getX() - 2, this.getY() + 5);     //Adds the word label into the world
        }
        timer--;     //Every act reduces the timer by one
        int num = (int)(timer/60) + 1;     //Division is used to create an Integer representing the time in seconds, rounded up
        time = Integer.toString(num);     //Integer time is converted to a String so that it can be displayed in the label
        if(timer < 0)
        //Checks if the bomb's time has run out
        {
            ((Level)getWorld()).lose();     //Activates the lose method in the main world
        }         
        displayTime.setImage(new GreenfootImage(time, 38, Color.BLACK, Color.WHITE));     //Updates the time left before the bomb explodes
    }

    /**
     * Removes the bomb and the labels created for it from the world
     */
    public void remove()
    {
        ((Level)getWorld()).removeObject(displayTime);
        ((Level)getWorld()).removeObject(displayWord);
        ((Level)getWorld()).removeObject(this);
    }

    /**
     * Returns the word associated with that bomb
     */
    public String getWord()
    {
        return word;
    }

    /**
     * Returns the length of the word associated with that bomb
     */
    public int wordLength()
    {
        return word.length();
    }
}

