import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The ExitButton class represents a button for the start screen
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class ExitButton extends Actor
{  
    public void act() 
    {        
        if(Greenfoot.mousePressed(this))
        //Checks to see if this object is pressed
        {
            Greenfoot.setWorld(new StartScreen());      //Sets the world to StartScreen
        }
    }    
}
