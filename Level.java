import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.*;
import greenfoot.Color;

/**
 * The Level class contains the main code for the game
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class Level extends World
{
    public String typed = "";     //Tracks what the user has typed
    public int counter = 50;     //Dictates when a bomb should be spawned
    public int bombIndex = 0;     //Tracks what index of the bomb ArrayList should be accessed
    public int score = 0;     //Tracks the user's score
    public int highScore;     //Keeps track of the high score
    public boolean egg = false; //Used to see if easter egg is activated
    ArrayList<Bomb> list = new ArrayList<Bomb>();     //Contains the bombs in the game
    Label display = new Label(typed, 80);    //Creates a labels for user input
    Label displayScore = new Label(score, 80);    //Creates a label for the score
    Label displayWord = new Label("", 52);     //Creates a label for the next word
    Words w = new Words();    //Creates an object of the word class. Used to obtain random words for the bombs

    /**
     * Constructor for the Level class
     * Tracks the high score and prepares the world
     */
    public Level(int highScore)
    {    
        super(800, 600, 1);     //Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        this.highScore = highScore;     //Tracks the user's high score
        prepare();     //Prepares the world by adding in the objects of the Label class
    }

    /**
     * Called every 60th of a second
     * Within this method:
     * - A bomb is created periodically
     * - The labels are updated
     * - What the user typed is registered
     */
    public void act()
    {
        counter--;     //Subtracts 1 from the bomb spawn counter
        Random random = new Random();     //Creates a new object of the random class
        //A bomb is created when counter reaches zero
        if(counter == 0)
        {
            String word = w.getWord();     //A random word is taken from the Words class
            int x = 100 + random.nextInt(600);     //A random x coordinate is set
            int y = 170 + random.nextInt(300);     //A random y coordinate is set
            list.add(new Bomb(word));     //A new bomb is created with the random word passed as a parameter
            counter = word.length() * 12 + 40;     //Counter is reset based on the length of the word
            word = "";     //Resets word to an empty String
            addObject(list.get(bombIndex), x, y);     //Places the created bomb in the world
            bombIndex++;     //Adjusts the bomb index for the bomb added
        }
        String key = Greenfoot.getKey();     //Takes the user input as a String
        if (key != null) 
        //Checks if a key is pressed
        {
            if(key.equals("enter"))
            //Checks to see if a word is being entered
            {
                for(int i = 0; i < list.size(); i++)
                //Iterates through each bomb in the bomb arraylist
                {
                    if(typed.equals(list.get(i).getWord()))
                    //Checks to see if word matches the bomb being checked
                    {
                        score += list.get(i).wordLength() * 100;     //The user's score is increased
                        list.get(i).remove();     //The bomb is removed from the world
                        list.remove(i);     //The bomb is removed from the ArrayList
                        bombIndex--;      //The bomb index is adjusted for the bomb removed
                    }
                    if(typed.equals("easteregg"))
                    {
                        egg = true;
                    }
                }
                typed = "";     //What the user has typed is reset to an empty String
            }

            else if (key.equals("backspace"))
            //Checks to see if the user is clicking the backspace
            {
                if(typed.length() > 0)
                //Checks if there is anything to remove
                {
                    typed = typed.substring(0, typed.length() - 1);     //The last character of the String is removed
                }
            }
            else
            //Enters the key(Not enter or backspace) into the typed display
            {
                for(int i = 97; i < 123; i++)
                {
                    //Iterates through the integer values for all lower case letters
                    if (key.charAt(0) == (char)i)
                    //Checks to see if this is the Character the user has typed
                    {
                        typed += key;     //Adds the Character to what the user has typed
                        break;
                    }
                }
            }
        }
        //The labels for the user input, score, and next word are updated
        display.setImage(new GreenfootImage(typed, 80, Color.BLACK, Color.WHITE));
        displayScore.setImage(new GreenfootImage("Score: " + Integer.toString(score), 70, Color.BLACK, Color.WHITE));
        displayWord.setImage(new GreenfootImage("Next word: " + w.seeWord(), 40, Color.BLACK, Color.WHITE));
    }

    /**
     * Ends the game
     * Plays an explosion sound and sets the world to the lose screen
     */
    public void lose()
    {
        Greenfoot.playSound("explosion.wav");     //Plays an explosion sound
        addObject(new kaBoom(), 400, 300);
        Greenfoot.delay(150);
        Greenfoot.setWorld(new LoseScreen(score, highScore));     //Sets the world to the lose screen
    }

    /**
     * Prepare the world for the start of the program
     * Creates the initial objects and add them to the world
     */
    private void prepare()
    {
        //Adds the three Label objects to the world
        addObject(display, 400, 550);
        addObject(displayScore, 160, 50);
        addObject(displayWord, 610, 40);
    }

    public boolean easterEgg()
    {
        // method to be used in rules to indicate if eastereggis activated
        return egg;
    }

}
