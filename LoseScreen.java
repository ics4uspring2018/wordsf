import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.HashMap;
import java.util.*;

/**
 * The LoseScreen class contains the code for the screen displayed after the user loses the game
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class LoseScreen extends World
{   
    Label displayMessage = new Label("", 80);     //Creates a label for the lose message
    Label displayHighScore = new Label("", 80);     //Creates a label for the high score
    public int score;     //Stores the user's score
    public int hScore;     //Stores the user's highScore
    
    /**
     * Constructor LoseScreen class
     * Takes the score and high score as parameters
     * Displays a message, updates the high score and adds two buttons to the screen
     */
    public LoseScreen(int score, int highScore)
    {    
        super(800, 600, 1);     // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        this.score = score;     //Sets the user's score
        hScore = highScore;     //Sets the user's highScore
        addObject(displayMessage, 400, 50);     //Adds the lose message label to the world
        displayMessage.setImage(new GreenfootImage("You Lose!", 80, Color.BLACK, Color.WHITE));     //Updates the message
        //If the user's score is higher than the highscore the highscore is updated 
        if(score > hScore)
        {
            hScore = score;
        }
        displayScore();
        addObject(new StartButton(hScore), 600, 500);     //Adds a start button
        addObject(new ExitButton(), 200, 500);     //Adds an exit button
    }
    
    /**
     * Adds the score display object the world
     */
    public void displayScore()
    {
        addObject(displayHighScore, 400, 150);    //Adds the score display object
        displayHighScore.setImage(new GreenfootImage("High Score: " + hScore, 80, Color.BLACK, Color.WHITE)); //Updates the score     
    }
}
