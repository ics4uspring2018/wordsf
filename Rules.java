import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Rules class contains the code that displays the rules for the user
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class Rules extends World
{

    Label displayRule = new Label("", 80); // Creates a label for rules
    Label displayExtra = new Label("", 80); // Creates a label for more writing
    /**
     * Constructor for objects of class Rules.
     * Adds an exit button and displays the rules for the game
     */
    public Rules()
    {    
        super(800, 600, 1);     // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        addObject(new ExitButton(), 500, 500);     //Adds an exit button to the world
        addObject(new RulesButton(), 400, 85);     //Adds a rules button to the world
        displayRule.setImage(new GreenfootImage("One Rule, Type the words on top of the bomb before the timer runs out ",30,Color.BLACK, Color.WHITE));// 
        addObject(displayRule, 400, 200); // displays the rule
        displayExtra.setImage(new GreenfootImage("*Easter Egg within the game  ",30,Color.BLACK, Color.WHITE));// 
        addObject(displayExtra, 400, 400); // displays the rule

    }

}
