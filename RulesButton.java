import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The RulesButton class represents a button for the rules screen
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class RulesButton extends Actor
{
    public void act() 
    {
        if(Greenfoot.mousePressed(this))
        //Checks to see if the object is pressed
        {
            Greenfoot.setWorld(new Rules());     //Sets the world to the rules screen
        }
    }    
}
