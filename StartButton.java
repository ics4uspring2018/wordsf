import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The StartButton class represents a button for the game screen
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class StartButton extends Actor
{
    int highScore;     //Stores the user's high score
   
    /**
     * Constructor for class StartButton
     * Tracks the user's high score
     */
    public StartButton(int highScore)
    {
        this.highScore = highScore;
    }
    
    public void act() 
    {
        if(Greenfoot.mousePressed(this))
        //Checks to see if this object is pressed
        {
            Greenfoot.setWorld(new Level(highScore));     //Sets the world to Level
        }
    }    
}
