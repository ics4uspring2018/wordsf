import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The StartScreen ains the main code for the game
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class StartScreen extends World
{
    /**
     * Constructor for class StartScreen.
     * Prepares the world by adding a decorative bomb, two buttons and a title
     */
    public StartScreen()
    {    
        // Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        prepare();
    }

    private void prepare()
    {
        addObject(new BombObject(), 205, 344);     //Adds a decorative bomb to the world
        addObject(new StartButton(0), 522, 350);     //Adds a start button to the world
        addObject(new RulesButton(), 500, 485);     //Adds a rules button to the world
        addObject(new Title(), 500, 170);     //Adds a title to the world
    }
}