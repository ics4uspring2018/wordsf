import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * The Words class is used to contain the words for the bombs
 * 
 * @authors Jonathan Hai, Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version April 2018
 */
public class Words extends Actor
{
    Reader r = new Reader();    //Creates an object of the Reader class to read the txt file
    Scanner wordInput = r.getScanner("AllWords.txt");     //Creates a Scanner to access the txt file
    Queue<String> q = new LinkedList<String>();     //Creates a Queue to store what word will be next
    ArrayList<String> words = new ArrayList<String>();     //Creates an ArrayList to store all of the words
    Random random = new Random();     //Creates an object of the random class

    /**
     * Constructor for the Words class
     * Puts all the words from the txt file into an ArrayList
     * Adds a word to the queue
     */
    public Words()
    {
        //Goes through the entire txt file and puts every line into the ArrayList of Strings
        while(wordInput.hasNextLine())
        {
            words.add(wordInput.nextLine());
        }
        addWord();     //Adds a random word to the queue
    }
    
    /**
     * Adds a new word to the queue
     * Removes a word from the queue and returns it
     */
    public String getWord()
    {
        addWord();
        return q.remove();
    }
    
    /**
     * Returns the next String in the queue
     */
    public String seeWord()
    {
        return q.peek();
    }
    
    /**
     * Adds a random word from the ArrayList to the queue
     */
    public void addWord()
    {
        int a = random.nextInt(words.size());
        String str = words.get(a);
        words.remove(a);
        q.add(str);
    }
}
